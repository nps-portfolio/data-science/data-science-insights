# 1. Questão analisada.

A Biblioteca do IFTM - Campus Paracatu possui um histórico de empréstimos com mais de 7 anos. Tais dados, nunca foram analisados para verificar se existem informações úteis para a gestão da biblioteca e/ou melhorias nos processos adotados pelo setor.

Dessa forma, esse trabalho se utiliza das técnicas de Data Science para identificar quais insigths podem ser conseguidos através dos dados de empréstimo entre os anos de 2015 e 2019. Também foram elaboradas algumas hipóteses para serem validadas/refutadas.

**Questão de negócio: De que modo os processos adotados atualmente no setor afetam a quantidade de empréstimos realizadas?**

Nesse sentido, as questões a serem verificadas são:

- O número médio de empréstimos realizados é considerado abaixo, acima ou na média?
- Quais insigths podem ser adquiridos dos dados de empréstimo?

# 2. Premissas e restrições.

-  Os dados foram coletados do GNUTECA no dia 21/06/2021
-  Estão sendo considerados o intervalo entre 2015 e 2019
-  Foram retirados os empréstimos:
        - ATRASADOS
        - PENDENTES
        - CHAVES DE ESCANINHO E SALAS DE ESTUDO

# 3. Planejamento da solução:

- Coleta de Dados
    - Retirar os dados do gnuteca e carregar para análise
- Limpar os dados (missing values, erros, inconsistências)
  Validar as hipóteses geradas
- Verificar os insigths
- Publicar a análisa no Heroku
- Apresentar para validar as informações encontradas
- Planejar melhorias

# 4. Tabela de hipóteses analisadas
| Hipótese | Resultado |
| ------ | ------ |
| **H1** - A quantidade de empréstimos aumentou em média 15% ano | Falsa |
| **H2** - Os livros de literatura representam 25% do total de empréstimos no período | Falsa |
| **H3** - Um usuário fica em média 10 dias com o livro emprestado | Falsa |
| **H4** - Mais de 10% dos livros emprestados são devolvidos com atraso | Verdadeira |
| **H5** - Usuários do sexo feminino pegam 30% MAIS livros que os usuários masculinos | Falsa |
| **H6** - Os livros de Ciências Sociais (Pedagógicos) representam menos de 5% do total de empréstimos | Verdadeira |
| **H7** - Em média 20% do acervo fica emprestado anualmente | Falsa |
| **H8** - O dia da semana com mais emprestimos é a quarta-feira | Falsa |
| **H9** - O mês do ano com mais empréstimo é junho | Falsa |
| **H10** - A média do valor das multas aplicadas por ano é superior à RS 1500,00 | Verdadeira |
| **H11** - Usuários do sexo M pagam em média 10% mais multas que os usuários do sexo F | Falsa |

# 5. Publicação da análise
- [Link da Análise](https://rocky-brushlands-03056.herokuapp.com/)

# Próximos passos

- Apresentar os resultados para o setor
- Verificar as novas hipóteses
- Identificar a criação de um modelo capaz de avaliar os livros doados (?)
